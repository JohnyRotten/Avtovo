#! /usr/ruby -w
# coding: utf-8
require 'sinatra'
require './helpers/storage.rb'
require './helpers/app.rb'
require './database/services.rb'

storage = Storage.new

enable :session

stats = {}


get '/' do
	@menu_items = {
		'#services'      => 'Услуги', 
		'#actions'       => 'Акции', 
		'#review'        => 'Отзывы', 
		'#sertification' => 'Сертификаты',
		'#clients'		 => 'Клиенты',
		'#partners'      => 'Партнёры', 
		'#go'            => 'Заказать', 
		'#map'           => 'Где нас найти'
	}
	
	@partners 	  = storage.partners
	@clients 	  = storage.clients
	@reviews 	  = storage.reviews
    @services 	  = storage.services
    @actions      = storage.actions 
    @sertificates = storage.sertificates
	@selects = storage.selects

	erb :landing, locals: { title: 'СТО "Автово" | Главная страница' } 
end

before '/admin/*'  do authenticate! end
before '/create/*' do authenticate! end
before '/delete/*' do authenticate! end
before '/change/*' do authenticate! end

post '/change/sertificate/:id' do |id|
	storage.sertificates.delete_at id
	storage.save_services
end


post '/delete/:cat/:id' do


	storage.save
end

post '/counter' do

end

get '/admin' do
	@menu_items = {
		'#orders'		   => 'Заказы',	
		'#statistics'      => 'Статистика',
		'#change_services' => 'Услуги', 
		'#change_actions'  => 'Акции', 
		'#change_review'   => 'Отзывы', 
		'#change_partners' => 'Партнёры', 
	}

	@partners 	  = storage.partners
	@reviews 	  = storage.reviews
    @services 	  = storage.services
    @actions      = storage.actions 
    @sertificates = storage.sertificates
    @orders 	  = storage.orders

	erb :admin, locals: { title: 'СТО "Автово" | Админка' }, layout: :admin
end

post '/send_alarm' do
	from_email = params["email"]
	to_email   = 'mr.alegon@yandex.ru'
	theme 	   = 'STO-Avtovo - Заказ'
	text       = "Вам поступил заказ на услугу.<br/>"
	text  <<  "Заказчик: #{params['lastname']} #{params['firstname']}<br/>"
	text  <<  "Номер телефона: #{params['tel']}<br/>"
	text  <<  "________________________<br/><br/>Личный автоответчик."
	# text  <<  params.to_s  << "<br/>"

	if send_mail from_email, to_email, theme, text
		storage.orders ||= []
		storage.orders << { name: params['lastname'] + ' ' + params[:firstname], email: params['email'], phone: params['tel'] }
		storage.save_orders
		"Заявка принята.<br/>Ждите, когда наш оператор свяжется с Вами."
	else
		"<p class='error'>ОШИБКА!</p><br>Непредвиденная ошибка при попытке оправить заявку.<br>Приносим свои извенения за неудобство."
	end
end

post '/record' do
	from_email = 'admin@avtovo.com'
	to_email   = 'mr.alegon@yandex.ru'
	theme 	   = 'STO-Avtovo - Заказ'
	text       = "Вам поступил заказ на обратный звонок.<br/>"
	text  <<  "Заказчик: #{params['name']} <br/>"
	text  <<  "Номер телефона: #{params['phone']}<br/>"
	text  <<  "________________________<br/><br/>Личный автоответчик."
	# text  <<  params.to_s  << "<br/>"
  begin
    if send_mail from_email, to_email, theme, text
      # storage.orders ||= []
      # storage.orders << { name: params['lastname'] + ' ' + params[:firstname], email: params['email'], phone: params['tel'] }
      # storage.save_orders
      "Заявка принята.<br/>Ждите, когда наш оператор свяжется с Вами."
    else
      "<p class='error'>ОШИБКА!</p><br>Непредвиденная ошибка при попытке оправить заявку.<br>Приносим свои извенения за неудобство."
    end
  rescue => er
	  "<p class='error'>ОШИБКА</p><br>#{er.message}"
  end
end

post '/counter/:param' do
	stats[params[:param]] ||= 0
	stats[params[:param]] = stats[params[:param]] + 1
	"#{p}: #{stats[params[:param]]}"
end

not_found do
	status 404
	@error = {
		code:    404,
		message: "Страница не найдена."
	}
	@menu_items = []
	erb :error, locals: { title: @error[:message] }
end

set :public_folder, File.dirname(__FILE__) + '/public'
