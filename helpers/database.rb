#! /usr/bin/ruby -w
# coding: utf-8

module Database

	def Database.get_services  
		# [
		# 	{
		# 		name: 'УАЗ',
		# 	  	description: 'Обслуживание автомобилей марки UAZ.',
		# 	  	img: "/images/uaz-patriot-04.jpg"
		# 	},
		# 	{
		# 		name: 'Ремонт',
		# 	  	description: 'Ремонт автомобилей и мототехники „под ключ”, работа со страховыми компаниями, ремонт в кредит.',
		# 	  	img: "/images/img0264.png" },
		# 	{
		# 		name: 'Кузовные работы',
		# 	  	description: 'Малярный и кузовной ремонт любой сложности, тонирование, шумоизоляция',
		# 	  	img: "/images/img0327.png"
		# 	},
		# 	{
		# 		name: 'Развал-схождение',
		# 	  	description: '',
		# 	  	img: "/images/img0372.png"
		# 	},
		# 	{
		# 		name: 'Шиномонтаж',
		# 	  	description: 'Шиномонтаж, прокатка дисков, хранение резины',
		# 	  	img: "/images/img0249.png"
		# 	},
		# 	{
		# 		name: 'Мойка, полировка кузова',
		# 	  	description: '',
		# 	  	img: "/images/img0326.png"
		# 	},
		# 	{
		# 		name: 'Дополнительные услуги',
		# 	  	description: 'Заправка, ремонт, диагностика кондиционеров, установка стекол, компьютерная диагностика, антикоррозионная обработка, установка обвеса, электротехнические работы, установка охранных и музыкальных систем любой сложности, эксклюзивная установка противоугонных секреток, установка XENON',
		# 	  	img: "/images/img0296.png"
		# 	}
		# ]
    [
        {
            type: 'Для физических лиц',
            items: [
                {
                    title: 'Слесарные работы',
                    description:
                        '<p>Техническое обслуживание(ТО)</p>
                        <p>Диагностика и ремонт ДВС</p>
                        <p>Диагностика и ремонт ходовой</p>
                        <p>Диагностика и ремонт агрегатов</p>
                        <p>Диагностика и ремонт тормозной системы</p>
                        <p>Экспрес замена масла</p>
                        <p>Развал/схождение'
                },
                {
                    title: 'Малярно-кузовные работы',
                    description:
                        '<p>Локальный ремонт</p>
                        <p>Замена и ремонт навесных элементов кузова</p>
                        <p>Ремонт бамперов</p>
                        <p>Устранение перекоса кузова</p>
                        <p>Устраненние перекоса рам</p>
                        <p>Покраска кузова</p>
                        <p>Тектильная обработка днища и кузова</p>
                        <p>Ремонт и замена несущих элементов кузова</p>
                        <p>Герметизация внутренних полостей кузова'
                },
                {
                    title: 'Арматурные работы',
                    description:
                        '<p>Замена всех элементов кузова</p>
                        <p>Замена стекол</p>
                        <p>Тонирование стекол/растонирование'
                },
                {
                    title: 'Электро-диагностические работы',
                    description:
                        '<p>Диагностика и ремонт электрооборудования</p>
                        <p>Установка воб. оборудования</p>
                        <p>Регулировка ...</p>
                        <p>Установка сигнализации</p>
                        <p>Заправка кондиционеров'
                },
                {
                    title: 'Шиномонтаж',
                    description:
                        '<p>Шиномонтаж R12 - R22</p>
                        <p>Ремонт железных дисков</p>
                        <p>Ремонт шин и балансировка</p>
                        <p>Хранение шин'
                },
                {
                    title: 'Мойка и ...',
                    description:
                        '<p>Мойка</p>
                        <p>Химчистка</p>
                        <p>Полировка</p>
                        <p>Дезинфекция</p>
                        <p>Эвакуация'
                },
            ]
        },
        {
            type: 'Для юридических лиц',
            items: [
                {
                    title: '',
                    description: ''
                },
            ]
        },
    ]
	end

	def Database.get_actions 
		[
			{ 
				description: 'Внимание!<br/> В этом сезоне владельцам УАЗ - скидка при полной переобувке в шиномонтаже 20%.',
				seconds: 59,
				minutes: 22,
				hours:   13
			},
			{ 
				description: 'Внимание!<br/> В этом сезоне владельцам УАЗ - скидка при полной переобувке в шиномонтаже 20%.',
				seconds: 9,
				minutes: 12,
				hours:   23
			}
		]
	end

	def Database.get_partners  
		[
			{ href: 'http://exist.ru', img: '/images/exist.ru.gif'}, #exist.ru
			{ href: 'http://emex.ru', img: '/images/emex.ru.png'}, #emex.ru
			{ href: '#', img: '/images/bmw.jpg'}, #MosorParts 
			{ href: '#', img: '/images/facebook.jpg'},  # EA.ru
			{ href: 'http://xenon78.ru', img: '/images/xenon78.ru.gif'}, # xenon78.ru
			{ href: '#', img: '/images/rolf-ford.ru.gif'}, # рольф-форд
			{ href: 'http://auto-help.ru', img: '/images/auto-help.ru.png'},  # auto-help.ru
			{ href: 'http://autotreid.ru', img: '/images/autotreid.ru.png'}, # autotreid.ru
			{ href: 'http://mikado.ru', img: '/images/mikado.ru.jpg'}, # mikado.ru
			{ href: 'http://kolesofortuny.ru', img: '/images/volvo.jpg'}, # kolesofortuny.ru
			{ href: 'http://shell.ru', img: '/images/shell.ru.png'}, # shell.ru
			{ href: 'http://www.vw-axsel.ru/', img: '/images/axel-city.png'}, # аксель сити юг
			{ href: 'http://brulex.ru', img: '/images/brulex.ru.png'}, # brulex.ru
		]
	end

	def Database.get_clients  
		[
			{ href: 'http://www.gefest.ru//', img: '/images/gefest.png'}, # гефсет
			{ href: 'http://www.ask-spb.com/', img: '/images/ack.gif'}, # АСК Петербург
			{ href: 'http://www.guideh.com/', img: '/images/gaide.gif'}, # Райде
			{ href: 'http://www.eurosibins.ru/', img: '/images/eurosib.png'}, # Евросиб
			{ href: 'http://www.chulpan.ru/', img: '/images/chulpan.gif'}, # татарская страховая компания
			{ href: 'http://www.i-f-brok.ru/', img: '/images/i-f-brok.ru.png/' }, # http://www.i-f-brok.ru/
			{ href: 'http://www.ulmart.ru/', img: '/images/ulmart.png' }, # ulmart.ru
			{ href: 'http://www.melston.ru/about/melston-service/', img: '/images/melston.png' }, # http://www.melston.ru/about/melston-service/
			{ href: 'http://www.geoizol.ru/', img: '/images/geoizol.png'}, # ОАО Теоизал
			{ href: 'http://www.vozr.ru/', img: '/images/vozr.png'}, # ОАО Возрождение
			{ href: 'http://www.intgr.ru/', img: '/images/intgr.png' }, # http://www.intgr.ru/
			{ href: 'http://cmy-13.ru/', img: '/images/smu-13.gif'}, # СМУ-13
			{ href: 'http://eurotransstroy.ru/', img: '/images/eurotransstroy.gif'}, # ОАО Евротрансстрой
		]
	end

	def Database.get_sertificates 
		[
			'/images/svid1.jpg',
			'/images/svid2.jpg',
			'/images/svid3.jpg',
			'/images/svid4.jpg',
			'/images/svid5.jpg',
			'/images/svid6.jpg',
			'/images/svid7.jpg',
			'/images/svid8.jpg',
			'/images/svid9.jpg',
		]
	end

	def Database.get_reviews 
		[
			{
				name: 'Светлана',
				image: '/images/photo1.jpg',
				review: 'Благо с правого фланга выполнил подачу углового, Никитин не сумел толком пробить головой, после чего заметался мяч в штрафной площади ростовчан, которые сумели разрядить ситуацию.'
			},
			{
				name: 'Владимир',
				image: '/images/photo2.jpg',
				review: 'Удалось пермякам зацепиться за мяч в середине поля, отодвинув игру от своих ворот.'
			},
			{
				name: 'Джейсон',
				image: '/images/photo3.jpg',
				review: 'И вот ремонт автомобиля требуется срочно, а денег на него в бюджете заложено не было. Что делать в этом случае? В этом случае предлагаем обратиться в автосервис АВТОВО, здесь вам сделают ремонт автомобиля в кредит.'
			},
			{
				name: 'Марина',
				image: '/images/photo1.jpg',
				review: 'Уважаемые клиенты! Автосервис АВТОВО предлагает вам новую услугу - Ремонт автомобиля и мототехники в кредит. Ни один владелец автомобиля не может быть уверенным, что с ним не приключится авария или автомобилю потребуется ремонт. Но посещение ремонтной мастерской дело в наши дни весьма дорогостоящее.'
			},
			{
				name: 'Двойник Владимира',
				image: '/images/photo2.jpg',
				review: 'Уважаемые клиенты! Автосервис АВТОВО предлагает вам новую услугу - Ремонт автомобиля и мототехники в кредит. Ни один владелец автомобиля не может быть уверенным, что с ним не приключится авария или автомобилю потребуется ремонт. Но посещение ремонтной мастерской дело в наши дни весьма дорогостоящее.'
			},
			{
				name: 'Странный парень',
				image: '/images/photo3.jpg',
				review: 'Удалось пермякам зацепиться за мяч в середине поля, отодвинув игру от своих ворот.'
			},
		]
	end

  def Database.get_selects
    [
        {
            img: '/images/technics.jpg',
            text: 'Профессиональное оборудование'
        },
        {
            img: '/images/square.png',
            text: 'Более 800 кв.м производственной площади'
        },
        {
            img: '/images/quality.jpg',
            text: 'Жесткий контроль технологий и качества'
        },
        {
            img: '/images/details.png',
            text: 'Широкий выбор оригинальных и неоригинальных запчастей'
        },
        {
            img: '/images/location.png',
            text: 'Удобное месторасположение'
        },
    ]
  end

end