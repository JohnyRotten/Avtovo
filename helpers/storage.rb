#! /usr/bin/ruby -w
# coding: utf-8
require './helpers/database.rb'

class Storage

	attr_accessor :services, :actions, :reviews, :clients, :partners, :sertificates, :orders, :selects

	def initialize
		load
	end

	def save
		save_services
		save_actions
		save_reviews
		save_partners
		save_clients
		save_sertificates
		save_orders
    save_selects
	end

	def save_services
		File.open('./tmp/services.out', 'w') do |file|
			file.write Marshal.dump @services
		end
	end

	def save_actions
		File.open('./tmp/actions.out', 'w') do |file|
			file.write Marshal.dump @actions
		end
	end

	def save_reviews
		File.open('./tmp/reviews.out', 'w') do |file|
			file.write Marshal.dump @reviews
		end
	end

	def save_partners
		File.open('./tmp/partners.out', 'w') do |file|
			file.write Marshal.dump @partners
		end
	end

	def save_clients
		File.open('./tmp/clients.out', 'w') do |file|
			file.write Marshal.dump @clients
		end
	end

	def save_sertificates
		File.open('./tmp/sertificates.out', 'w') do |file|
			file.write Marshal.dump @sertificates
		end
	end

	def save_orders
		File.open('./tmp/orders.out', 'w') do |file|
			file.write Marshal.dump @orders
		end
  end

  def save_selects
    File.open('./tmp/selects.out', 'w') do |file|
      file.write Marshal.dump @selects
    end
  end

	def load
		@services     = Database.get_services#Marshal.load File.open('./tmp/services.out',     'r')
		@actions      = Database.get_actions#Marshal.load File.open('./tmp/actions.out',      'r')
		@reviews      = Database.get_reviews#Marshal.load File.open('./tmp/reviews.out',      'r')
		@partners     = Database.get_partners#Marshal.load File.open('./tmp/partners.out',     'r')
		@clients 	  = Database.get_clients#Marshal.load File.open('./tmp/clients.out', 	'r')
		@sertificates = Database.get_sertificates#Marshal.load File.open('./tmp/sertificates.out', 'r')
		@orders       = Marshal.load File.open('./tmp/orders.out',       'r')
	    @selects      = Database.get_selects#Marshal.load File.open('./tmp/selects.out',      'r')
	rescue
	end
end