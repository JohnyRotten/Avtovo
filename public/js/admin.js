$(function(){
	
	function module(str) {
		$('body').append('<div class="module"><div class="text"></div><a class="close big-btn" href="#go">Окей</a></div>');
		$('.module .text').html(str);
		$('.module').css('margin-top', -$('.module').height()/2);
		$('.module').animate({'left': '0'}, 300);
	}

	$('body').on('click', '.module .close', function(){
		var parent = $('.module');
		var after  =  $('.module:after');
		var before =  $('.module:before');
		
		parent.animate({'left': '100%'}, 300);

		setTimeout(function() { parent.remove(); }, 300);

		return false;
	});

	$('body').on('click', 'nav a', function(){
		var href = $(this).attr('href');
	    $('html, body').animate({'scroll-top': $(href).offset().top - 60 }, 500);
		return false;
	});
});