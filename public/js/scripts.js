$(function(){

	setInterval(function(){ $('.slider .next').trigger('click'); }, 15000);

	// function checkPositions() {
	// 	$('#top').height($(window).height());

	// 	var offset =  (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
	// 	var navHeight = $('#header nav').height();
	// 	var windowHeight = $(window).height();
	// 	// var top = (offset < windowHeight) ? (windowHeight - offset - navHeight): navHeight + 40;
	// 	var top;
	// }

	// checkPositions();

	(function() {
		$('#header').addClass('animated bounceInDown');
		$('.top-row:first').addClass('animated bounceInLeft');
		$('#mini_form').parent().addClass('animated bounceInRight');
	})();

	$('#select ul li img').hover(function(){
		$(this).animate({
			'border-radius': 20
		}, 500);
	}, function(){
		$(this).animate({
			'border-radius': 1000
		}, 500);
	});

	function sleep(ms) {
		ms += new Date().getTime();
		while (new Date() < ms){}
	} 

	function module(str) {
		$('body').append('<div class="module"><div class="text"></div><a class="close big-btn" href="#go">Окей</a></div>');
		$('.module .text').html(str);
		$('.module').addClass('animated rotateIn');
	}

	$('body').on('click', '.module .close', function(){
		var parent = $(this).parent();
		parent.addClass('animated rotateOut');
		setTimeout(function(){ parent.remove(); }, 1000);
		return false;
	});

	$('body').on('click', '.min-img', function(){
		var src    = $(this).attr('src');
		var relat;
		$('body').append('<div id="moduleimg"><img src="' + src + '"/></div>');
		$('#moduleimg').animate({ opacity: 1 }, 300);
		sleep(300);

		var width  = $('#moduleimg img').width();
		var height = $('#moduleimg img').height();
		
		if (width  > $(window).width()) {
			relat  = $(window).width() / width;
			width  = $(window).width();
			height = Math.round(height * relat); 
		}
		console.log('image before: ' + width + 'x' + height);
		if (height > $(window).height()) {
			relat  = $(window).height() / height;
			height = $(window).height();
			width  = Math.round(width * relat);
		}
		console.log('window: ' + $(window).width() + 'x' + $(window).height());
		console.log('image after: ' + width + 'x' + height);
		relat && console.log('Relat: ' + relat);
		$('#moduleimg img').css({
			width: width,
			height: height,
			marginTop: Math.round(-height / 2),
			marginLeft: Math.round(-width / 2)
		});
	});

	$('body').on('click', '#moduleimg', function(){
		$(this).animate({ 'opacity': 0 }, 300);
		setTimeout(function() { $('#moduleimg').remove(); }, 300);
	});

	$('body').on('click', 'a', function(){
		var href = $(this).attr('href');
		if (/^#/.test(href)) {
			var speed = 1000;
	        var top = $(href).offset().top;
	        $('html, body').animate({scrollTop: top}, speed);
			href = href.replace(/[#]/i, '');
		}
		href = '/counter/' + href;
	
		$.ajax({
			url: href,
			method: 'post'
		});

		return false;
	});

	$('body').on('click', '#to_top', function(){
        $('html, body').animate({scrollTop: 0}, 300);
        // $(this).addClass('animated fadeOut');
		return false;
	});

	$('.min-img, .partners img').hover(function(){
		$(this).animate({'opacity': 1}, 200);
	}, function(){
		$(this).animate({'opacity': 0.7}, 200);
	});

	$(document).resize(function(){
		// checkPositions();
	});

	$(document).scroll(function(){
		// checkPositions();

	});

	/* Sending form */
	$('body').on('submit', '#go form', function(){
		var url 	  = $(this).attr('action');
		var reg_tel   = /[^\D]/;
		var reg_email = /^([a-z]+[\w\-\._]*)@([a-z]+[\w\-\._]*\.[a-zA-Z]{2,4})$/i;
		var reg_name  = /^[a-zа-я]+$/i;

		var number 		  = $('input[name="phone"]').val();
		var email_address = $('input[name="email"]').val();
		var first_name 	  = $('input[name="first_name"]').val();
		var last_name 	  = $('input[name="last_name"]').val();

		var checked = true;
		var errors = [];

		if (!reg_name.test(last_name)) {
			errors[errors.length] = "Неправильно/не заполнено фамилия.";
			checked = false;
		}
		if (!reg_name.test(first_name)) {
			errors[errors.length] = "Неправильно/не заполнено имя.";
			checked = false;
		}
		if (!reg_tel.test(number)) {
			errors[errors.length] = "Неправильно набран номер.";
			checked = false;
		}
		if (!reg_email.test(email_address)) {
			errors[errors.length] = "Неправильно заполнено поле email.";
			checked = false;
		}
			
		if (checked) {
			$.ajax({
				url: url,
				method: 'post',
				data: {
					tel: number,
					email: email_address,
					firstname: first_name,
					lastname: last_name 
				}
			}).done(function(data){
				module(data);
			});
		} else {
			module("<b class='error'>Ошибка!</b><br>- " + errors.join('<br>- '));
		}
		return false;
	});

	$('body').on('submit', '#mini_form', function(){
		var url 	  = $(this).attr('action');
		var reg_tel   = /[^\D]/;
		var reg_name  = /^[a-zа-я]+$/i;

		var number 		  = $('input[name="phone"]').val();
		var name 	  = $('input[name="name"]').val();

		var errors = [];

		if (!reg_name.test(name)) {
			errors[errors.length] = "Неправильно/не заполнено поле Имя.";
		}
		if (!reg_tel.test(number)) {
			errors[errors.length] = "Неправильно набран номер.";
		}
			
		if (!errors.length) {
			$.ajax({
				url: url,
				method: 'post',
				data: {
					tel: number,
					name: name 
				}
			}).done(function(data){
				module(data);
			});
		} else {
			module("<b class='error'>Ошибка!</b><br>- " + errors.join('<br>- '));
		}
		return false;
	});

	/* Counter of actions */
	if ($('.actions li').length) {
		var count_actions = []
		$('.actions li').each(function(index, element){
			count_actions[count_actions.length] = setInterval(function(){
				var hours   = parseInt($(element).find('.hd').text() + $(element).find('.ha').text());
				var minutes = parseInt($(element).find('.md').text() + $(element).find('.ma').text());
				var seconds = parseInt($(element).find('.sd').text() + $(element).find('.sa').text()) - 1;
				if (seconds < 0) {
					minutes--;
					seconds = 59;
				}
				if (minutes < 0) {
					hours--;
					minutes = 59;
				}
				$(element).find('.hd').text((hours - hours % 10) / 10);
				$(element).find('.ha').text(hours % 10);
				$(element).find('.md').text((minutes - minutes % 10) / 10);
				$(element).find('.ma').text(minutes % 10);
				$(element).find('.sd').text((seconds - seconds % 10) / 10);
				$(element).find('.sa').text(seconds % 10);
				if (!hours && !minutes && !seconds) {
					clearInterval(count_actions[index]);
					module('Акция закончена.');
					$(element).remove();
				}
			}, 1000);
		});
	}

	/* Slider */
	$('body').on('click', '.slider .prev', function(){
		var current = $(this).parent().data('current') - 1;
		var imgs    = $(this).parent().children('img');
		if (current < 0) current = imgs.size() - 1;
		$(this).parent().data('current', current);
		imgs.css('display', 'none');
		$('.slider img:nth-child(' + (current + 1) + ')').css('display', 'block').addClass('animated bounceInDown');
		return false;
	});
	$('body').on('click', '.slider .next', function(){
		var current = $(this).parent().data('current') + 1;
		var imgs    = $(this).parent().children('img');
		current %= imgs.size();
		$(this).parent().data('current', current);
		imgs.css('display', 'none');
		$('.slider img:nth-child(' + (current + 1) + ')').css('display', 'block').addClass('animated bounceInDown');
		return false;
	});
});