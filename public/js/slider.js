$(function(){
	if (!$('#slider').length) return;

	var width = 960;
	var count = $('.sliders li').length;
	var now = 0;
	var std_delay = 300;

	function offset() {
		$('.sliders').animate({opacity: 0}, 250);
		setTimeout(function() {
			$('.sliders').css({left: - now*width}).data('current', now);
			$('.sliders').animate({opacity: 1}, 250);
		}, 250);
	}

	setInterval(function(){	$('#slider .next').trigger('click'); }, 10000);

	$('#slider').append('<div class="next">></div><div class="prev"><</div>');

	$('#slider').on('click', '.prev', function(){
		!!now ? now-- : now = count - 1;
		offset();
	});
	$('#slider').on('click', '.next', function(){
		now < count - 1 ? now++ : now = 0;
		offset();
	});
});